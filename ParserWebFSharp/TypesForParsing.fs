namespace ParserWeb

open System
open System.Collections.Generic

type Arguments =
    | IrkutskOil
    | Akd
    | Lsr
    | Butb
    | RosSel
    | Neft
    | Slav
    | Aero
    | StroyTorgi
    | Asgor
    | GosYakut
    | RosTend
    | ChPt
    | Tplus
    | SibServ
    | TGuru
    | BidMart

type SlavNeft =
    | MEGION
    | YANOS
    | NGRE

type Exist =
    | Exist
    | NoExist

type RosSelRec =
    { Href : string
      PurNum : string }

type NeftRec =
    { Href : string
      PurNum : string
      PurName : string
      OrgName : string
      DatePub : DateTime
      DateEnd : DateTime }

type SlavNeftRec =
    { HrefDoc : string
      HrefName : string
      PurNum : string
      PurName : string
      OrgName : string
      CusName : string
      DatePub : DateTime
      DateEnd : DateTime
      status : string
      typeT : SlavNeft }

type StroyTorgiRec =
    { Url : string
      PurNum : string
      PurName : string
      OrgName : string
      Status : string
      Price : string
      Currency : string }

type AeroRec =
    { Href : string
      PurNum : string
      PurName : string
      PwayName : string
      DatePub : DateTime
      DateEnd : DateTime
      status : string }

type AsgorRec =
    { Href : string
      PurNum : string
      PurName : string
      OrgName : string
      CusName : string
      DatePub : DateTime
      DateEnd : DateTime
      status : string
      PwayName : string
      Nmck : string
      NameLots : string Set }

type GosYakutRec =
    { Href : string
      PurNum : string
      PurName : string
      CusName : string
      CusUrl : string
      DatePub : DateTime
      DateEnd : DateTime
      PwayName : string
      Nmck : string }

type RosTendRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      Region : string
      Nmck : string
      DelivPlace : string
      Currency : string
      Page : string }

type ChPtRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      DateEnd : DateTime
      Nmck : string
      Currency : string }

type TPlusRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      DateEnd : DateTime
      Nmck : string
      Status : string
      region : string
      Page : string
      Exist : Exist }

[<Struct>]
type DocSibServ =
    { name : string
      url : string }

type SibServRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      DateEnd : DateTime
      DocList : List<DocSibServ> }

type TGuruRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      DateEnd : DateTime
      Nmck : string
      OrgName : string
      RegionName : string }

type BidMartRec =
    { Href : string
      PurNum : string
      PurName : string
      DatePub : DateTime
      DateEnd : DateTime
      Nmck : string
      Quant : string }
